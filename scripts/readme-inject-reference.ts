import { readFileSync, writeFileSync } from "fs"
import { resolve } from "path"

const BEGIN_MARKER = "<!--- BEGIN-INJECT-REFERENCE --->"
const END_MARKER = "<!--- END-INJECT-REFERENCE --->"

console.log("Injecting dist/index.d.ts in README.md")

const readmeFile = resolve(__dirname, "../README.md")
const readme = readFileSync(readmeFile).toString()

const dtsFile = resolve(__dirname, "../dist/index.d.ts")
const dts = readFileSync(dtsFile).toString()
const markdown = "```ts\n" + dts + "\n```\n"

const start = readme.substring(0, readme.indexOf(BEGIN_MARKER) + BEGIN_MARKER.length)
const end = readme.substring(readme.indexOf(END_MARKER))

writeFileSync(readmeFile, `${start}\n${markdown}${end}`)
