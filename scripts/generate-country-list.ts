import { writeFileSync } from "fs"
import { resolve } from "path"
import { getAlpha2Codes } from "i18n-iso-countries"

console.log("Generating: ISO Alpha2 Country Codes")

const destFile = resolve(__dirname, "../src/lists/iso-country-alpha2.generated.ts")
const data = Object.keys(getAlpha2Codes())

writeFileSync(destFile, `export const codes = ${JSON.stringify(data, null, "  ")} as const`)
