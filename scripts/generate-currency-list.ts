import { writeFileSync } from "fs"
import { resolve } from "path"
import * as currencies from "currency-codes"

console.log("Generating table: ISO Alpha3 Currency Codes")

const destFile = resolve(__dirname, "../src/lists/iso-currency-alpha3.generated.ts")
const data = currencies.data.map((c: any) => c.code)

writeFileSync(destFile, `export const codes = ${JSON.stringify(data, null, "  ")} as const`)
