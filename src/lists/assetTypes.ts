/**
 * - "M" = Monetary / ISO currency
 * - "S" = Security / by ISIN
 * - "C" = Crypto
 */
export const assetTypes = ["M", "S", "C"] as const
