import { BrandedType } from "./BrandedType"

export type IsoDate = BrandedType<"IsoDate", string>
