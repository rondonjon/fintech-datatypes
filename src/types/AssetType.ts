import { assetTypes } from "../lists/assetTypes"

export type AssetType = typeof assetTypes[number]
