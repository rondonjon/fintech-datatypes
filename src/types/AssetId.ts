import { BrandedType } from "./BrandedType"

/**
 * Combines an `AssetType`, a colon as separator, and a type-depending asset code.
 * Example: `${assetType}:${code}`
 */
export type AssetId = BrandedType<"assetId", string>
