import { codes } from "../lists/iso-currency-alpha3.generated"

export type IsoCurrencyA3 = typeof codes[number]
