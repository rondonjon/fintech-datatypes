import { BrandedType } from "./BrandedType"

export type IsoTime = BrandedType<"IsoTime", string>
