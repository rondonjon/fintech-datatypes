import { BrandedType } from "./BrandedType"

export type WKN = BrandedType<"WKN", string>
