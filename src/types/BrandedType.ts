/**
 * Generic implementation of "Branded Primitives" for increased
 * type-safety in applications.
 */
export type BrandedType<Brand extends string, BaseType> = BaseType & {
  readonly __brand: Brand
}
