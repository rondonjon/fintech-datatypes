import { BrandedType } from "./BrandedType"

/**
 * A custom type to represent arbitrary-precision numbers as strings.
 *
 * The library enforces no constraints on the values and provides no
 * custom typeguard to leave the choice of an implementation to the
 * application developer. We recommend `big.js`.
 */
export type BigDecimal = BrandedType<"BigDecimal", string>
