import { codes } from "../lists/iso-country-alpha2.generated"

export type IsoCountryA2 = typeof codes[number]
