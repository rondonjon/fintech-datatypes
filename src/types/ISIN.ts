import { BrandedType } from "./BrandedType"

export type ISIN = BrandedType<"ISIN", string>
