import { assetTypes } from "../lists/assetTypes"
import { AssetType } from "../types/AssetType"

export const isAssetType = (s: string): s is AssetType => assetTypes.includes(s as AssetType)
