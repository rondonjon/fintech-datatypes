import { codes } from "../lists/iso-country-alpha2.generated"
import { IsoCountryA2 } from "../types/IsoCountryA2"

export const isIsoCountryA2 = (s: string): s is IsoCountryA2 => codes.includes(s as IsoCountryA2)
