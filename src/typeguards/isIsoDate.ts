import { IsoDate } from "../types/IsoDate"

/**
 * Tests if the given `string` matches the `YYYY-MM-DD` notation of the ISO<sup>TM</sup> 8601
 * specification and its value is within the permitted bounds.
 *
 * See https://www.iso.org/iso-8601-date-and-time-format.html
 */
export const isIsoDate = (s: string): s is IsoDate => {
  const match = s && s.match(/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/)

  if (!match) {
    return false
  }

  const parsedDate = new Date(s)

  if (Number.isNaN(parsedDate.getTime())) {
    return false
  }

  const calendarDay = parsedDate.toISOString().substring(0, 10)

  if (calendarDay !== s) {
    // captures invalid calendar dates such as 2020-02-31
    return false
  }

  return true
}
