import { isAssetId } from "./isAssetId"

describe("isAssetId()", () => {
  test("positives", () => {
    expect(isAssetId("M:EUR")).toBe(true)
    expect(isAssetId("S:DE0005140008")).toBe(true)
    expect(isAssetId("C:BTC")).toBe(true)
  })

  test("negatives", () => {
    expect(isAssetId("EUR")).toBe(false)
    expect(isAssetId("M:eur")).toBe(false)
    expect(isAssetId("m:EUR")).toBe(false)
    expect(isAssetId("S:XX0005140008")).toBe(false)
    expect(isAssetId("C:")).toBe(false)
    expect(isAssetId("foo:bar")).toBe(false)
    expect(isAssetId("S:")).toBe(false)
  })
})
