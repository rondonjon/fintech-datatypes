import { isIsoDate } from "./isIsoDate"

describe("isIsoDate()", () => {
  test("positive", () => {
    expect(isIsoDate("2020-02-17")).toBe(true)

    const isoDay = new Date("2020-01-01").toISOString().substring(0, 10)
    expect(isIsoDate(isoDay)).toBe(true)
  })

  test("negative", () => {
    expect(isIsoDate("")).toBe(false)
    expect(isIsoDate("100")).toBe(false)
    expect(isIsoDate("100-02-17")).toBe(false)
    expect(isIsoDate("2000-2-17")).toBe(false)
    expect(isIsoDate("2000-02-7")).toBe(false)
    expect(isIsoDate("0100-1-1")).toBe(false)
    expect(isIsoDate("2000-00-01")).toBe(false)
    expect(isIsoDate("2000-01-00")).toBe(false)
    expect(isIsoDate("2000-01-00")).toBe(false)
    expect(isIsoDate("2000/01/01")).toBe(false)
  })
})
