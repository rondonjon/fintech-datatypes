import { isIsoCountryA2 } from "./isIsoCountryA2"

describe("isIsoCountryA2()", () => {
  test("positives", () => {
    expect(isIsoCountryA2("DE")).toBe(true)
    expect(isIsoCountryA2("FR")).toBe(true)
  })

  test("negatives", () => {
    expect(isIsoCountryA2(" DE")).toBe(false)
    expect(isIsoCountryA2("DE ")).toBe(false)
    expect(isIsoCountryA2("D")).toBe(false)
    expect(isIsoCountryA2("De")).toBe(false)
    expect(isIsoCountryA2("de")).toBe(false)
    expect(isIsoCountryA2("XY")).toBe(false)
    expect(isIsoCountryA2("")).toBe(false)
  })
})
