import { codes } from "../lists/iso-currency-alpha3.generated"
import { IsoCurrencyA3 } from "../types/IsoCurrencyA3"

export const isIsoCurrencyA3 = (s: string): s is IsoCurrencyA3 => codes.includes(s as IsoCurrencyA3)
