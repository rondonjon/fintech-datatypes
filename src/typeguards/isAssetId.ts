import { AssetId } from "../types/AssetId"
import { isAssetType } from "./isAssetType"
import { isIsin } from "./isIsin"
import { isIsoCurrencyA3 } from "./isIsoCurrencyA3"

export const isAssetId = (s: string): s is AssetId => {
  const type = s.charAt(0)

  if (!isAssetType(type)) {
    return false
  }

  const code = s.substring(2)

  if (type === "M") {
    return isIsoCurrencyA3(code)
  } else if (type === "S") {
    return isIsin(code)
  } else {
    return code.match(/^[A-Z]{1,5}$/) !== null
  }
}
