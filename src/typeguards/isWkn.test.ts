import { isWkn } from "./isWkn"

describe("isWkn()", () => {
  test("positive", () => {
    expect(isWkn("514000")).toBe(true)
  })

  test("negative", () => {
    expect(isWkn("")).toBe(false)
    expect(isWkn(" ")).toBe(false)
    expect(isWkn("51400")).toBe(false)
    expect(isWkn(" 514000")).toBe(false)
    expect(isWkn("514000 ")).toBe(false)
    expect(isWkn("_14000")).toBe(false)
    expect(isWkn("#14000")).toBe(false)
  })
})
