import { isIsoTime } from "./isIsoTime"

describe("isIsoTime()", () => {
  test("positive", () => {
    expect(isIsoTime("01:57")).toBe(true)
    expect(isIsoTime("23:57")).toBe(true)
    expect(isIsoTime("00:00")).toBe(true)
  })

  test("negative", () => {
    expect(isIsoTime("")).toBe(false)
    expect(isIsoTime("1:23")).toBe(false)
    expect(isIsoTime("23:1")).toBe(false)
    expect(isIsoTime("24:00")).toBe(false)
  })
})
