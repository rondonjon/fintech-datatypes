import { ISIN } from "../types/ISIN"
import { isIsoCountryA2 } from "./isIsoCountryA2"

export const calculateCheckDigit = (isinCountryCode: string, nsin: string) => {
  const s = isinCountryCode + nsin
  const mapIsinDigits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  let m: string = ""

  // Translate characters to numbers

  for (let i = 0; i < s.length; i++) {
    m += mapIsinDigits.indexOf(s[i])
  }

  // Set the parity mode

  const parity = m.length & 1

  // Weight the digits, based on parity, and concatenate the results

  let n: string = ""

  for (let i = 0; i < m.length; i++) {
    n += Number(m[i]) * ((i & 1) === parity ? 1 : 2)
  }

  // Compute the cross-sum

  let o = 0

  for (let i = 0; i < n.length; i++) {
    o += Number(n[i])
  }

  // Double Modulo 10

  return (10 - (o % 10)) % 10
}

export const isIsin = (s: string): s is ISIN => {
  const parts = s?.match(/^([A-Z]{2})([0-9A-Z]{9})([0-9]{1})$/)

  if (!parts) {
    // Syntax error
    return false
  }

  const [, cc, nsin, cd] = parts
  const PSEUDO_COUNTRY_CODES = ["XS", "XA", "XB", "XC", "XD", "XF", "QS", "QT", "QW", "EU"]

  if (!isIsoCountryA2(cc) && !PSEUDO_COUNTRY_CODES.includes(cc)) {
    // Invalid country code
    return false
  }

  const ccd = calculateCheckDigit(cc, nsin)

  if (Number(cd) !== ccd) {
    // Invalid check digit
    return false
  }

  return true
}
