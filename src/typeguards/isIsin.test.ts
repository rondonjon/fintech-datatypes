import { isIsin } from "./isIsin"

describe("isIsin()", () => {
  const ISIN_APPLE = "US0378331005"
  const ISIN_DEUTSCHE_BANK = "DE0005140008"
  const ISIN_TREASURY_CORP_VICTORIA = "AU0000XVGZA3"

  test("Invalid Syntax", () => {
    expect(isIsin("DE000514")).toBe(false)
    expect(isIsin("DE000514000X")).toBe(false)
    expect(isIsin("")).toBe(false)
    expect(isIsin("1")).toBe(false)
  })

  test("Invalid Country Codes", () => {
    expect(isIsin("XE0005140008")).toBe(false)
    expect(isIsin("de0005140008")).toBe(false)
  })

  test("Check Digit", () => {
    expect(isIsin("DE0005140007")).toBe(false)
    expect(isIsin("DE0005140009")).toBe(false)
  })

  test("Valid ISINs", () => {
    expect(isIsin(ISIN_APPLE)).toBe(true)
    expect(isIsin(ISIN_TREASURY_CORP_VICTORIA)).toBe(true)
    expect(isIsin(ISIN_DEUTSCHE_BANK)).toBe(true)
  })
})
