import { isIsoCurrencyA3 } from "./isIsoCurrencyA3"

describe("isIsoCurrencyA3()", () => {
  test("positives", () => {
    expect(isIsoCurrencyA3("EUR")).toBe(true)
    expect(isIsoCurrencyA3("USD")).toBe(true)
  })

  test("negatives", () => {
    expect(isIsoCurrencyA3("€")).toBe(false)
    expect(isIsoCurrencyA3("eur")).toBe(false)
    expect(isIsoCurrencyA3("euro")).toBe(false)
    expect(isIsoCurrencyA3("usd")).toBe(false)
    expect(isIsoCurrencyA3("$")).toBe(false)
  })
})
