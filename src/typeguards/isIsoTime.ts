import { IsoTime } from "../types/IsoTime"

/**
 * Tests if the given `string` matches the `HH:MM` notation of the ISO<sup>TM</sup> 8601
 * specification and its value is within the permitted bounds.
 *
 * See https://www.iso.org/iso-8601-date-and-time-format.html
 */
export const isIsoTime = (s: string): s is IsoTime => {
  const match = s.match(/^([0-2][0-9]):[0-5][0-9]$/)
  return match !== null && Number(match[1]) <= 23
}
