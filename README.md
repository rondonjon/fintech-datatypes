# rondonjon-fintech-datatypes

A collection of TypeScript datatypes and typeguards for use in FinTech applications.

## Reference

<!--- BEGIN-INJECT-REFERENCE --->
```ts
/**
 * Generic implementation of "Branded Primitives" for increased
 * type-safety in applications.
 */
declare type BrandedType<Brand extends string, BaseType> = BaseType & {
    readonly __brand: Brand;
};

/**
 * Combines an `AssetType`, a colon as separator, and a type-depending asset code.
 * Example: `${assetType}:${code}`
 */
declare type AssetId = BrandedType<"assetId", string>;

/**
 * - "M" = Monetary / ISO currency
 * - "S" = Security / by ISIN
 * - "C" = Crypto
 */
declare const assetTypes: readonly ["M", "S", "C"];

declare type AssetType = typeof assetTypes[number];

/**
 * A custom type to represent arbitrary-precision numbers as strings.
 *
 * The library enforces no constraints on the values and provides no
 * custom typeguard to leave the choice of an implementation to the
 * application developer. We recommend `big.js`.
 */
declare type BigDecimal = BrandedType<"BigDecimal", string>;

declare type ISIN = BrandedType<"ISIN", string>;

declare const codes$1: readonly ["AF", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV", "BR", "IO", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CG", "CD", "CK", "CR", "CI", "HR", "CU", "CY", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GN", "GW", "GY", "HT", "HM", "VA", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IL", "IT", "JM", "JP", "JO", "KZ", "KE", "KI", "KP", "KR", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "MP", "MK", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RE", "RO", "RU", "RW", "SH", "KN", "LC", "PM", "VC", "WS", "SM", "ST", "SA", "SN", "SC", "SL", "SG", "SK", "SI", "SB", "SO", "ZA", "GS", "ES", "LK", "SD", "SR", "SJ", "SZ", "SE", "CH", "SY", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB", "US", "UM", "UY", "UZ", "VU", "VE", "VN", "VG", "VI", "WF", "EH", "YE", "ZM", "ZW", "AX", "BQ", "CW", "GG", "IM", "JE", "ME", "BL", "MF", "RS", "SX", "SS", "XK"];

declare type IsoCountryA2 = typeof codes$1[number];

declare const codes: readonly ["AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN", "BAM", "BBD", "BDT", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BOV", "BRL", "BSD", "BTN", "BWP", "BYN", "BZD", "CAD", "CDF", "CHE", "CHF", "CHW", "CLF", "CLP", "CNY", "COP", "COU", "CRC", "CUC", "CUP", "CVE", "CZK", "DJF", "DKK", "DOP", "DZD", "EGP", "ERN", "ETB", "EUR", "FJD", "FKP", "GBP", "GEL", "GHS", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "ILS", "INR", "IQD", "IRR", "ISK", "JMD", "JOD", "JPY", "KES", "KGS", "KHR", "KMF", "KPW", "KRW", "KWD", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL", "LYD", "MAD", "MDL", "MGA", "MKD", "MMK", "MNT", "MOP", "MRU", "MUR", "MVR", "MWK", "MXN", "MXV", "MYR", "MZN", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "OMR", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG", "QAR", "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SDG", "SEK", "SGD", "SHP", "SLL", "SOS", "SRD", "SSP", "STN", "SVC", "SYP", "SZL", "THB", "TJS", "TMT", "TND", "TOP", "TRY", "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "USN", "UYI", "UYU", "UYW", "UZS", "VES", "VND", "VUV", "WST", "XAF", "XAG", "XAU", "XBA", "XBB", "XBC", "XBD", "XCD", "XDR", "XOF", "XPD", "XPF", "XPT", "XSU", "XTS", "XUA", "XXX", "YER", "ZAR", "ZMW", "ZWL"];

declare type IsoCurrencyA3 = typeof codes[number];

declare type IsoDate = BrandedType<"IsoDate", string>;

declare type IsoTime = BrandedType<"IsoTime", string>;

declare type WKN = BrandedType<"WKN", string>;

declare const isAssetId: (s: string) => s is AssetId;

declare const isAssetType: (s: string) => s is "M" | "S" | "C";

declare const isIsin: (s: string) => s is ISIN;

declare const isIsoCountryA2: (s: string) => s is "AF" | "AL" | "DZ" | "AS" | "AD" | "AO" | "AI" | "AQ" | "AG" | "AR" | "AM" | "AW" | "AU" | "AT" | "AZ" | "BS" | "BH" | "BD" | "BB" | "BY" | "BE" | "BZ" | "BJ" | "BM" | "BT" | "BO" | "BA" | "BW" | "BV" | "BR" | "IO" | "BN" | "BG" | "BF" | "BI" | "KH" | "CM" | "CA" | "CV" | "KY" | "CF" | "TD" | "CL" | "CN" | "CX" | "CC" | "CO" | "KM" | "CG" | "CD" | "CK" | "CR" | "CI" | "HR" | "CU" | "CY" | "CZ" | "DK" | "DJ" | "DM" | "DO" | "EC" | "EG" | "SV" | "GQ" | "ER" | "EE" | "ET" | "FK" | "FO" | "FJ" | "FI" | "FR" | "GF" | "PF" | "TF" | "GA" | "GM" | "GE" | "DE" | "GH" | "GI" | "GR" | "GL" | "GD" | "GP" | "GU" | "GT" | "GN" | "GW" | "GY" | "HT" | "HM" | "VA" | "HN" | "HK" | "HU" | "IS" | "IN" | "ID" | "IR" | "IQ" | "IE" | "IL" | "IT" | "JM" | "JP" | "JO" | "KZ" | "KE" | "KI" | "KP" | "KR" | "KW" | "KG" | "LA" | "LV" | "LB" | "LS" | "LR" | "LY" | "LI" | "LT" | "LU" | "MO" | "MG" | "MW" | "MY" | "MV" | "ML" | "MT" | "MH" | "MQ" | "MR" | "MU" | "YT" | "MX" | "FM" | "MD" | "MC" | "MN" | "MS" | "MA" | "MZ" | "MM" | "NA" | "NR" | "NP" | "NL" | "NC" | "NZ" | "NI" | "NE" | "NG" | "NU" | "NF" | "MP" | "MK" | "NO" | "OM" | "PK" | "PW" | "PS" | "PA" | "PG" | "PY" | "PE" | "PH" | "PN" | "PL" | "PT" | "PR" | "QA" | "RE" | "RO" | "RU" | "RW" | "SH" | "KN" | "LC" | "PM" | "VC" | "WS" | "SM" | "ST" | "SA" | "SN" | "SC" | "SL" | "SG" | "SK" | "SI" | "SB" | "SO" | "ZA" | "GS" | "ES" | "LK" | "SD" | "SR" | "SJ" | "SZ" | "SE" | "CH" | "SY" | "TW" | "TJ" | "TZ" | "TH" | "TL" | "TG" | "TK" | "TO" | "TT" | "TN" | "TR" | "TM" | "TC" | "TV" | "UG" | "UA" | "AE" | "GB" | "US" | "UM" | "UY" | "UZ" | "VU" | "VE" | "VN" | "VG" | "VI" | "WF" | "EH" | "YE" | "ZM" | "ZW" | "AX" | "BQ" | "CW" | "GG" | "IM" | "JE" | "ME" | "BL" | "MF" | "RS" | "SX" | "SS" | "XK";

declare const isIsoCurrencyA3: (s: string) => s is "AED" | "AFN" | "ALL" | "AMD" | "ANG" | "AOA" | "ARS" | "AUD" | "AWG" | "AZN" | "BAM" | "BBD" | "BDT" | "BGN" | "BHD" | "BIF" | "BMD" | "BND" | "BOB" | "BOV" | "BRL" | "BSD" | "BTN" | "BWP" | "BYN" | "BZD" | "CAD" | "CDF" | "CHE" | "CHF" | "CHW" | "CLF" | "CLP" | "CNY" | "COP" | "COU" | "CRC" | "CUC" | "CUP" | "CVE" | "CZK" | "DJF" | "DKK" | "DOP" | "DZD" | "EGP" | "ERN" | "ETB" | "EUR" | "FJD" | "FKP" | "GBP" | "GEL" | "GHS" | "GIP" | "GMD" | "GNF" | "GTQ" | "GYD" | "HKD" | "HNL" | "HRK" | "HTG" | "HUF" | "IDR" | "ILS" | "INR" | "IQD" | "IRR" | "ISK" | "JMD" | "JOD" | "JPY" | "KES" | "KGS" | "KHR" | "KMF" | "KPW" | "KRW" | "KWD" | "KYD" | "KZT" | "LAK" | "LBP" | "LKR" | "LRD" | "LSL" | "LYD" | "MAD" | "MDL" | "MGA" | "MKD" | "MMK" | "MNT" | "MOP" | "MRU" | "MUR" | "MVR" | "MWK" | "MXN" | "MXV" | "MYR" | "MZN" | "NAD" | "NGN" | "NIO" | "NOK" | "NPR" | "NZD" | "OMR" | "PAB" | "PEN" | "PGK" | "PHP" | "PKR" | "PLN" | "PYG" | "QAR" | "RON" | "RSD" | "RUB" | "RWF" | "SAR" | "SBD" | "SCR" | "SDG" | "SEK" | "SGD" | "SHP" | "SLL" | "SOS" | "SRD" | "SSP" | "STN" | "SVC" | "SYP" | "SZL" | "THB" | "TJS" | "TMT" | "TND" | "TOP" | "TRY" | "TTD" | "TWD" | "TZS" | "UAH" | "UGX" | "USD" | "USN" | "UYI" | "UYU" | "UYW" | "UZS" | "VES" | "VND" | "VUV" | "WST" | "XAF" | "XAG" | "XAU" | "XBA" | "XBB" | "XBC" | "XBD" | "XCD" | "XDR" | "XOF" | "XPD" | "XPF" | "XPT" | "XSU" | "XTS" | "XUA" | "XXX" | "YER" | "ZAR" | "ZMW" | "ZWL";

/**
 * Tests if the given `string` matches the `YYYY-MM-DD` notation of the ISO<sup>TM</sup> 8601
 * specification and its value is within the permitted bounds.
 *
 * See https://www.iso.org/iso-8601-date-and-time-format.html
 */
declare const isIsoDate: (s: string) => s is IsoDate;

/**
 * Tests if the given `string` matches the `HH:MM` notation of the ISO<sup>TM</sup> 8601
 * specification and its value is within the permitted bounds.
 *
 * See https://www.iso.org/iso-8601-date-and-time-format.html
 */
declare const isIsoTime: (s: string) => s is IsoTime;

/**
 * Tests if the given `string` represents a valid `WKN`.
 *
 * Note: the requirements for valid WKNs hav been relaxed several times, with the latest definition
 * being that [any composition of six digits and/or capital letters qualifies as WKN](https://en.wikipedia.org/wiki/Wertpapierkennnummer).
 */
declare const isWkn: (s: string) => s is WKN;

export { AssetId, AssetType, BigDecimal, BrandedType, ISIN, IsoCountryA2, IsoCurrencyA3, IsoDate, IsoTime, WKN, isAssetId, isAssetType, isIsin, isIsoCountryA2, isIsoCurrencyA3, isIsoDate, isIsoTime, isWkn };

```
<!--- END-INJECT-REFERENCE --->

## Acknowledgements

- `BrandedType` datatype based on &quot;Branded Primitives&quot; in the
  [Typescript Documentation](https://github.com/microsoft/TypeScript/wiki/FAQ#can-i-make-a-type-alias-nominal)
- `isIsin()` implements a checksum validation based on
  [Wikipedia examples](https://en.wikipedia.org/wiki/International_Securities_Identification_Number)
  and a pseudo-country list from the [`isin-validator`](https://www.npmjs.com/package/isin-validator)
  library (MIT-licensed, Copyright © 2019 Victor Korzunin)
- `isIsoCountryA2()` relies on a minified datatable that is derived from the country
  lists in the [`i18n-iso-countries`](https://www.npmjs.com/package/i18n-iso-countries)
  library (MIT-licensed, Copyright © 2016 widdix GmbH)
- `isIsoCurrencyA3()`relies on a minified datatable that is derived from the currency
  [`currency-codes`](https://www.npmjs.com/package/currency-codes)
  library (MIT-licensed, Copyright © 2018-present Tobias Baunbæk Christensen)

## License

Licensed under the terms of the MIT License.

Copyright © 2022 "Ron Don Jon" Daniel Kastenholz

## History

| Version | Changes                                                  |
| ------- | -------------------------------------------------------- |
| 1.0.0   | Initial version                                          |
| 1.0.1   | Add repository URL to `package.json`, add README details |
| 1.0.2   | Fix `types` field in `package.json`                      |
| 1.0.3   | Remove unused dependency                                 |
| 1.0.4   | Update `.npmignore`                                      |
| 1.0.5   | Add Reference section to `README.md`                     |
| 1.0.6   | Exclude `.prettierignore` in `.npmignore`                |
| 1.0.7   | Extend documentation                                     |
| 1.0.8   | Add copyright notices to `README.md`                     |
